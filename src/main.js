import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VModal from 'vue-js-modal'

import './assets/scss/main.scss';
import router from './router'

Vue.use(VModal);

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
